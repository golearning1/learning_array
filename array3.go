package main

import "fmt"

func main() {

	var ismlar = [...]string{"Muhammad", "Mustafo", "Paygambarim", "bek", "ali"}
	fmt.Println(len(ismlar))
	fmt.Println(cap(ismlar))
	ismlar[0] = "Muhammad"
	ismlar[1] = "Mustafo"
	ismlar[2] = "Salollohu"
	ismlar[3] = "Alayhi"
	ismlar[4] = "Vassallam"
	fmt.Printf("Paygambarim: %s\n", ismlar[0])

	massiv := [...]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 23, 34, 45, 54, 5, 66}
	fmt.Println("Array Length: ", len(massiv))
	yigindi := 0
	for i := 0; i < len(massiv); i++ {
		fmt.Println("Son: ", massiv[i])
		yigindi += massiv[i]

	}
	fmt.Println("Yig'indi: ", yigindi)
}
