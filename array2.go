   //Array. Array elementidagi sonlarni qushib yigindini hisoblovchi dastur

package main

import "fmt"

func main(){

	myArray :=[...]int{111,333,222,444,555,666,765,789}
	fmt.Println("UZUNLIGI: ",len(myArray))
	fmt.Println("SIGIMI: ",cap(myArray))
	yigindi := 0
	for i := 0; i < len(myArray); i++{
		fmt.Println("Son: ",myArray[i])
		yigindi += myArray[i]
	}
	fmt.Println("Yig'indi",yigindi)

	firma :=[...]string{"ferrari","mustang","lomborgini"}
	fmt.Println(firma)
	firma[0] ="uzDaewoo"
	fmt.Println(firma)
	fmt.Printf("uzbek: %s",firma[0])

}