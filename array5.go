//range da qoshish,kopaytirish,ortalamasini hisoblab beruvchi dastur
package main

import "fmt"

func main() {

	sonlar := [...]int{23, 45, 66, 22, 12, 11}
	yigindi := 0
	faktariyal := 1
	ortalama := 0
	for _, v := range sonlar {
		fmt.Println("SON: ", v)
		yigindi += v
		faktariyal *= v
		ortalama = yigindi / 6
	}
	fmt.Println("YIGINDI: ", yigindi)
	fmt.Println("FAKTARIYAL: ", faktariyal)
	fmt.Println("ORT.ARFIMETIGI ", ortalama)

}
