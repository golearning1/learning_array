  //ARRAY. array elementi ichidagi yozuvlarni ekranga chiqarish 

package main

import "fmt"

func main() {

	myArray := [3]int{20, 21, 22}
	fmt.Println(myArray)

	fmt.Printf("Son1: %d\n", myArray[0])
	fmt.Printf("Son2: %d\n", myArray[1])
	fmt.Printf("Son3: %d\n", myArray[2])

	var familyalar [3]string
	fmt.Println(familyalar)
	familyalar[0] = "Husanov"
	familyalar[1] = "Qarshiyev"
	familyalar[2] = "Jumayev"
	fmt.Printf("Familya1: %s\n", familyalar[0])
	fmt.Printf("Familya2: %s\n", familyalar[1])
	fmt.Printf("Familya3: %s\n", familyalar[2])
	// Aynan qaysi biri kerak menga
	fmt.Printf("Familya2: %s\n", familyalar[1])

	var number = [4]int{10, 11, 12, 13}
	fmt.Println("Array Length: ", len(number))

}
