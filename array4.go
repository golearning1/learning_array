   // Array range bilan ishlsh

package main

import "fmt"

func main() {

	davlatlar := [...]string{"UZBEKISTAN", "AMERIKA", "GERMANIYA", "DUBAY"}
	for i, v := range davlatlar {
		fmt.Println("INDEX:", i, "VALUE:", v)

	}

	sonlar := [...]int{12, 34, 54, 31, 23, 45, 76, 89, 98, 55, 46, 39, 44, 66}
	for _, v := range sonlar{
		if v%2 == 0 {
			fmt.Println("JUFT SON:", v)
		} else {
			fmt.Println("TOQ SON:", v)
		}

	}
}
